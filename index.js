const express = require("express");
const app = express();

const port = 8080;


app.use(express.static("public"));

app.get("/",function(req,res){
    res.sendFile(__dirname + "index.html");
});

app.listen(port,function() {
    console.log("Serv working on " +port);
});
